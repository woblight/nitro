# Nitro

Displays movement speed in World of Warcraft.

## Resizing/moving the frame

Send `/nitro unlock` in chat to move/resize the frame, `/nitro lock` to hide the resize handle.

## Requiremens

EmeraldFramework https://gitlab.com/woblight/EmeraldFramework
