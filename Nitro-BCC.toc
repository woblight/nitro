## Interface: 20501
## Title: Nitro
## Notes: Speed meter
## Version: 0.1
## Author: WobLight
## SavedVariables: NitroSettings
## SavedVariablesPerCharacter: NitroProfile
## Dependencies: EmeraldFramework
Nitro.lua
