local addonName, addonTable = ...

_G[addonName] = addonTable

setmetatable(addonTable, {__index = getfenv() })
setfenv(1, addonTable)

local defaultSettings = {}
local BASE_MOVEMENT_SPEED = BASE_MOVEMENT_SPEED or 7

handler = EFrame.Object()
handler:attach("scale")
handler.__scale = 1

local eventHandler = CreateFrame("frame")
eventHandler:RegisterEvent("ADDON_LOADED")
eventHandler:SetScript("OnEvent", function(self, event, ...) EFrame:atomic(self[event], ...) end)

anchor = EFrame.MouseArea()
anchor.width = EFrame.bind(function() return handler.scale * 1024 end)
anchor.height = EFrame.bind(anchor, "width")
anchor.tex = EFrame.Rectangle(anchor)
anchor.tex.anchorFill = anchor
anchor.tex.borderWidth = 2
anchor.tex.borderColor = {.25,1,1}
anchor.tex.color = {.25,1,1,0.1}
anchor.dragTarget = anchor
anchor.visible = false

anchor.scalingHandle = EFrame.Button(anchor)
anchor.scalingHandle.icon = "Interface\\Addons\\EmeraldFramework\\Textures\\ResizeHandle_small"
anchor.scalingHandle.width=14
anchor.scalingHandle.height=14
anchor.scalingHandle.anchorBottom = anchor.bottom
anchor.scalingHandle.anchorRight = anchor.right
anchor.scalingHandle:connect("pressedChanged", function (p)
    local handle = anchor.scalingHandle
    if p then
        handle.ox = handle.mouseX
        handle.oy = handle.mouseY
    end
end)
anchor.scalingHandle:connect("mouseXChanged", function (x)
    local handle = anchor.scalingHandle
    if handle.__pressed then
        handler.scale = math.max((anchor.__width + (x - handle.ox)) / 1024, 0.2)
    end
end)

meter = EFrame.Item()
meter.anchorFill = anchor

meter.texTop = EFrame.Image(meter)
meter.texTop.anchorTopLeft = meter.topLeft
meter.texTop.width = EFrame.bind(function() return meter.width * 0.5 end)
meter.texTop.height = EFrame.bind(function() return meter.height * 0.5 end)
meter.texTop.source = "Interface\\AddOns\\Nitro\\nitro1"
meter.texBot = EFrame.Image(meter)
meter.texBot.anchorBottomLeft = meter.bottomLeft
meter.texBot.width = EFrame.bind(function() return meter.width * 0.5 end)
meter.texBot.height = EFrame.bind(function() return meter.height * 0.5 end)
meter.texBot.source = "Interface\\AddOns\\Nitro\\nitro2"

function MakeIndicator(unit, color, z)
    local indicator1
    indicator1 = EFrame.Image(meter)
    indicator1:attach("speed")
    indicator1.__speed = 0
    indicator1.source = "Interface\\AddOns\\Nitro\\NitroIndicator"
    indicator1.z = z
    indicator1.width = EFrame.bind(function() return meter.width * 0.09 end)
    indicator1.height = EFrame.bind(indicator1, "width")
    indicator1.color = color
    indicator1.anchorCenter = meter.center
    indicator1.hoffset = EFrame.bind(function() return - meter.width * 0.385 * math.cos(-indicator1.speed/BASE_MOVEMENT_SPEED * math.pi/4 + math.pi/4 * 1.2) end)
    indicator1.voffset = EFrame.bind(function() return - meter.width * 0.385 * math.sin(-indicator1.speed/BASE_MOVEMENT_SPEED * math.pi/4 + math.pi/4 * 1.2) end)
    indicator1.rotation = EFrame.bind(function() return indicator1.speed/BASE_MOVEMENT_SPEED * math.pi/4 - math.pi/4 * 1.2 end)
    EFrame.root:connect("update", function ()
        local a = 0
        if indicator1.lastUpdate then
            a = math.min((GetTime() - indicator1.lastUpdate)/0.125, 1)
        else
            a = 0
        end
        indicator1.lastUpdate = GetTime()
        indicator1.speed = indicator1.speed * (1-a) + a * GetUnitSpeed(unit)
        indicator1.visible = UnitExists(unit)
    end)
    return indicator1
end
indicator1 = MakeIndicator("player", {1,0,0,.66}, 5)
indicator2 = MakeIndicator("target", {0,0,0,.66}, 4)

function eventHandler.ADDON_LOADED(name)
    if name == addonName then
        if not NitroProfile then
            _G.NitroProfile = _G.NitroProfile or {}
        end
        setmetatable(NitroProfile, {__index = defaultSettings})
        
        if not NitroProfile.x then
            anchor.marginLeft = EFrame.bind("(EFrame.root.width - self.width)/2")
            anchor.marginTop =  EFrame.bind("(EFrame.root.height - self.height)/2")
        else
            anchor.marginLeft = NitroProfile.x
            anchor.marginTop = NitroProfile.y
        end
        anchor.anchorTopLeft = EFrame.root.topLeft
        anchor:connect("marginLeftChanged", function(x) NitroProfile.x = x end)
        anchor:connect("marginTopChanged", function(y) NitroProfile.y = y end)
        
        if NitroProfile.scale then
            handler.scale = NitroProfile.scale
        end
        handler:connect("scaleChanged", function(s) NitroProfile.scale = s end)
    end
end

local function chatcmd(msg)
    if msg == "unlock" then
        anchor.visible = true
    elseif msg == "lock" then
        anchor.visible = false
    end
end

_G.SLASH_NITRO1 = "/nitro"
_G.SlashCmdList["NITRO"] = chatcmd
